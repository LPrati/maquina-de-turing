# ==========================================================
# turing_machine.py: Contém as classes que implementam o
# comportamento da Máquina de Turing (TuringMachine, TuringTape),
# bem como classes auxiliares para inicializar a Máquina de Turing
# com os parâmetros desejados (StateTransitions, TuringMachineFactory).
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

class TuringMachine(object):
    def __init__(self, acceptance_state_index, state_transitions, tape_to_process):
        self.initial_state_index = 0
        self.acceptance_state_index = acceptance_state_index
        self.state_transitions = state_transitions

        self.head_position = 1
        self.current_state = 0

        self.tape = TuringTape(tape_to_process)


    def process_tape(self):
        while self.current_state != self.acceptance_state_index:
            read_char = self.tape.read_symbol(self.head_position)

            if self.current_state in self.state_transitions.keys():
                if read_char in self.state_transitions[self.current_state].keys():

                    to_write_char = self.state_transitions[self.current_state][read_char].get_char_to_write()
                    self.tape.write_symbol(to_write_char, self.head_position)

                    self.head_position += self.state_transitions[int(self.current_state)][read_char].get_movement_direction()

                    self.current_state = self.state_transitions[int(self.current_state)][read_char].get_final_state()

                else:
                    return False
            else:
                return False
        return True

class TuringTape(object):
    def __init__(self, string):
        self.tape = list(string)

    def write_symbol(self, symbol, index):
        self.tape[index] = symbol

    def read_symbol(self, index):
        return self.tape[index]

    def get_tape(self):
        return self.tape
     

class StateTransition(object):
    def __init__(self, final_state, char_to_write, movement_direction):
        self.final_state = final_state
        self.char_to_write = char_to_write
        self.movement_direction = movement_direction
    
    def get_char_to_write(self):
        return self.char_to_write
    
    def get_final_state(self):
        return self.final_state

    def get_movement_direction(self):
        return self.movement_direction


class TuringMachineFactory(object):
    def __init__(self):
        pass

    def make_machine(self, acceptance_state_index, tape_to_process, transitions_list):
        state_transitions = self.build_state_transitions_dict(transitions_list)
        tape_to_process = "B" + tape_to_process + "B"
        machine = TuringMachine(acceptance_state_index, state_transitions, tape_to_process)
        return machine

    
    def build_state_transitions_dict(self, transitions_list):
        movement_map = {
            "L": -1,
            "S": 0,
            "R": 1
        }
        state_transitions = {}
        for state_transition in transitions_list:
            [initial_state, read_char, final_state, write_char, movement_direction] = list(state_transition.split())
            if int(initial_state) not in state_transitions.keys():
                state_transitions[int(initial_state)] = {}
            state_transitions[int(initial_state)][read_char] = StateTransition(int(final_state), write_char, movement_map[movement_direction])

        return state_transitions

def test():
    num_estados = 6
    simbolos_terminais = ["a", "b", "c"]
    alfabeto_extendido = ["#", "*", "@", "B"]
    estado_aceitacao = 5
    transicoes = [
        "0 a 1 # R",
        "1 # 1 # R",
        "1 a 1 a R",
        "1 * 1 * R",
        "1 b 2 * R",
        "2 b 2 b R",
        "2 @ 2 @ R",
        "2 c 3 @ L",
        "3 # 3 # L",
        "3 * 3 * L",
        "3 @ 3 @ L",
        "3 b 3 b L",
        "3 a 1 # L",
        "3 B 4 B R",
        "4 # 4 # R",
        "4 * 4 * R",
        "4 @ 4 @ R",
        "4 B 5 B R"
    ]
    cadeias = [
        "BabbccaB",
        "BaabbccB",
        "BbacB",
        "BaaabbbccccB",
        "B-B",
        "BabcabcB",
        "BabcB",
        "BabccB",
        "BcB",
        "BaaabbbbcccB"
    ]

    factory = TuringMachineFactory()
    for index, cadeia in enumerate(cadeias):
        maquina = factory.make_machine(estado_aceitacao, cadeia, transicoes)
        resultado = "Aceita" if maquina.process_tape() else "Rejeita"
        print(f"{index+1}. {resultado}")


if __name__ == "__main__":
    test()