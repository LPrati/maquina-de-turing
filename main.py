# ==========================================================
# main.py: Script principal, que deve ser invocado para
# executar o simulador de Máquinas de Turing.
#
# Forma de utilizacao: 
#   
#    $python3 main.py <arquivo_entrada> <nome_saida>
#
# <arquivo_entrada> é o caminho para um arquivo de texto
# no formato da especificacao do projeto.
#
# <nome_saida> é o caminho do arquivo que será criado pelo
# programa contendo os resultados no formato da especificacao
# do projeto.
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

import sys

from turing_machine import TuringMachine, TuringMachineFactory
from parse_input import ParseInput

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(f"Número de argumentos inválidos: 2 esperados, {len(sys.argv)-1} encontrados")
        print("")
        print("Forma de utilizacao do programa (se estiver utilizando um executável .exe):")
        print("SimuladorMaquinaDeTuring.exe <arquivo_de_entrada> <nome_arquivo_saida>")
        print("")
        print("Forma de utilizacao do programa (se estiver utilizando um script .py):")
        print("python3 main.py <arquivo_de_entrada> <nome_arquivo_saida>")
        print("")
        print("Argumentos: ")
        print("<arquivo_de_entrada> é o caminho para um arquivo existente no formato da especificacao do projeto.")
        print("<nome_arquivo_saida> é nome do arquivo que o programa criará com os resultados.")
        exit(0)
    arquivo_entrada = sys.argv[1]
    arquivo_saida = sys.argv[2]

    dados_entrada = ParseInput(arquivo_entrada)

    resultados = []
    factory = TuringMachineFactory()

    for cadeia in dados_entrada.cadeias:
        turing_machine = factory.make_machine(
            dados_entrada.indice_estado_aceitacao, 
            cadeia, 
            dados_entrada.transicoes
        )
        if turing_machine.process_tape() is True:
            resultados.append("aceita")
        else:
            resultados.append("rejeita")

    with open(arquivo_saida, "w") as f:
        for index, resultado in enumerate(resultados):
            f.write(f"{resultado}")
            if index != len(resultados) - 1:
                f.write("\n")