# ==========================================================
# input.py: Contém uma classe responsável por fazer a leitura
# do arquivo de entrada e interpretar as linhas, recuperando
# as informacoes que serão utilizadas na simulacao.
#
# Autores:
#   Álefe Alves Silva
#   Felipe Sampaio Amorim
#   Márcio G V Silva
#   Leonardo Giovanni Prati
#   Michelle Schmitt Gmurczyk
# ==========================================================

class ParseInput:
    def __init__(self, path_arquivo_entrada: str) -> None:
        self.path_arquivo_entrada = path_arquivo_entrada
        self.dados_entrada = []
        self.num_estados = 0
        self.len_alfabeto_terminal = 0
        self.alfabeto_terminal = []
        self.len_alfabeto_extendido = 0
        self.alfabeto_extendido = []
        self.num_estados_iniciais = 0
        self.indice_estado_aceitacao = []
        self.num_transicoes = 0
        self.transicoes = []
        self.num_cadeias = 0
        self.cadeias = []

        self._parse()

    
    def _abre_arquivo(self) -> None:
        with open(self.path_arquivo_entrada, "r") as f:
            self.dados_entrada = f.readlines()
        self.dados_entrada = [line.strip('\n') for line in self.dados_entrada]

    def _le_num_estados(self):
        self.num_estados = int(self.dados_entrada[0])

    def _le_alfabeto_terminal(self):
        self.len_alfabeto_terminal = int(self.dados_entrada[1].split()[0])
        self.alfabeto_terminal = [char for char in self.dados_entrada[1].split()[1:]]
    
    def _le_alfabeto_extendido(self):
        self.len_alfabeto_extendido = int(self.dados_entrada[2].split()[0])
        self.alfabeto_extendido = [char for char in self.dados_entrada[2].split()[1:]]

    def _le_estado_de_aceitacao(self):
        self.indice_estado_aceitacao = int(self.dados_entrada[3])

    def _le_transicoes(self):
        self.num_transicoes = int(self.dados_entrada[4])
        for i in range(5, 5 + self.num_transicoes):
            transicao = self.dados_entrada[i]
            self.transicoes.append(transicao)
    
    def _le_cadeias(self):
        self.num_cadeias = int(self.dados_entrada[5 + self.num_transicoes])
        for i in range(6 + self.num_transicoes, 6 + self.num_transicoes + self.num_cadeias ):
            self.cadeias.append(self.dados_entrada[i])

    def _parse(self):
        self._abre_arquivo()
        self._le_num_estados()
        self._le_alfabeto_terminal()
        self._le_alfabeto_extendido()
        self._le_estado_de_aceitacao()
        self._le_transicoes()
        self._le_cadeias()